#include "threads.h"

void I_Love_Threads()
{
	cout << "I Love Threads\n" << endl;
}

void call_I_Love_Threads()
{
	std::thread t1(I_Love_Threads);
	t1.join();
}

void printVector(vector<int> primes)
{
	if (!primes.empty())
	{
		for (std::vector<int>::iterator it = primes.begin(); it != primes.end(); ++it)
		{
			cout << *it << endl;
		}
	}
}

void getPrimes(int begin, int end, vector<int>& primes)
{
	int i = 0;
	int j = 2;
	int flag = 0;

	for (i = begin; i <= end; i++) 
	{
		flag = 0;
		for (j = 2; j < i; j++) 
		{
			if (i%j == 0)
			{
				flag = 1;
				break;
			}
		}
		if (flag == 0) {//if the x = 1 flag is never set the number is prime.
			primes.push_back(i);
		}
	}

	//printVector(primes);
}

vector<int> callGetPrimes(int begin, int end)
{
	vector<int> primes;
	double start = omp_get_wtime();

	std::thread t1(getPrimes,begin,end,ref(primes));
	t1.join();
	double stop = omp_get_wtime();
	double sec = stop - start;
	cout << "Time: " << sec << endl;

	return primes;
}

void writePrimesToFile(int begin, int end, ofstream & file)
{
	int i = 0;
	int j;
	int flag;
	for (i = begin; i <= end; i++)
	{
		flag = 0;
		for (j = 2; j < i; j++)
		{
			if (i%j == 0)
			{
				flag = 1;
				break;
			}
		}
		if (flag == 0) {//if the x = 1 flag is never set the number is prime.
			file << i;
			file << "\n";
		}
	}
	
}

void callWritePrimesMultipleThreads(int begin, int end, string filePath, int N)
{
	int i = 0;
	std::thread myThreads[100];
	ofstream files;
	files.open(filePath);
	double start = omp_get_wtime();
	for (i = 0; i < N; i++)
	{
		myThreads[i] = std::thread (writePrimesToFile, begin, end, ref(files));
	}

	for (i = 0; i < N; i++)
	{
		myThreads[i].join();
	}

	files.close();
	double stop = omp_get_wtime();
	double sec = stop - start;
	cout << "Time: " << sec << endl;
	
}


